package br.com.senac.calculadora;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView txtResultado;
    private TextView txtHistorico;

    private Button btnNum1;
    private Button btnNum2;
    private Button btnNum3;
    private Button btnNum4;
    private Button btnNum5;
    private Button btnNum6;
    private Button btnNum7;
    private Button btnNum8;
    private Button btnNum9;
    private Button btnNum0;

    private Button btnSoma;
    private Button btnSubtracao;
    private Button btnMultiplicacao;
    private Button btnDivisao;
    private Button btnIgual;
    private Button btnCE;
    private Button btnPonto;

    private double primeiroNum = 0;
    private double segundoNum = 0;
    private String sinal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtResultado = findViewById(R.id.txtResultado);
        txtHistorico = findViewById(R.id.txtHistorico);

        btnNum1 = findViewById(R.id.btnNum1);
        btnNum2 = findViewById(R.id.btnNum2);
        btnNum3 = findViewById(R.id.btnNum3);
        btnNum4 = findViewById(R.id.btnNum4);
        btnNum5 = findViewById(R.id.btnNum5);
        btnNum6 = findViewById(R.id.btnNum6);
        btnNum7 = findViewById(R.id.btnNum7);
        btnNum8 = findViewById(R.id.btnNum8);
        btnNum9 = findViewById(R.id.btnNum9);
        btnNum0 = findViewById(R.id.btnNum0);

        btnSoma = findViewById(R.id.btnSoma);
        btnSubtracao = findViewById(R.id.btnSubtracao);
        btnMultiplicacao = findViewById(R.id.btnMultiplicacao);
        btnDivisao = findViewById(R.id.btnDivisao);
        btnIgual = findViewById(R.id.btnIgual);
        btnCE = findViewById(R.id.btnCE);
        btnPonto = findViewById(R.id.btnPonto);


        btnNum1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (txtResultado.getText().toString().equals("0")) {

                    txtResultado.setText("");
                    txtResultado.setText(txtResultado.getText().toString() + "1");

                } else {
                    txtResultado.setText(txtResultado.getText().toString() + "1");
                }
            }
        });

        btnNum2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txtResultado.getText().toString().equals("0")) {
                    txtResultado.setText("");
                    txtResultado.setText(txtResultado.getText().toString() + "2");
                } else {
                    txtResultado.setText(txtResultado.getText().toString() + "2");
                }
            }
        });

        btnNum3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txtResultado.getText().toString().equals("0")) {
                    txtResultado.setText("");
                    txtResultado.setText(txtResultado.getText().toString() + "3");
                } else {
                    txtResultado.setText(txtResultado.getText().toString() + "3");
                }
            }
        });

        btnNum4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txtResultado.getText().toString().equals("0")) {
                    txtResultado.setText("");
                    txtResultado.setText(txtResultado.getText().toString() + "4");
                } else {
                    txtResultado.setText(txtResultado.getText().toString() + "4");
                }
            }
        });

        btnNum5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txtResultado.getText().toString().equals("0")) {
                    txtResultado.setText("");
                    txtResultado.setText(txtResultado.getText().toString() + "5");
                } else {
                    txtResultado.setText(txtResultado.getText().toString() + "5");
                }
            }
        });

        btnNum6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txtResultado.getText().toString().equals("0")) {
                    txtResultado.setText("");
                    txtResultado.setText(txtResultado.getText().toString() + "6");
                } else {
                    txtResultado.setText(txtResultado.getText().toString() + "6");
                }
            }
        });

        btnNum7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txtResultado.getText().toString().equals("0")) {
                    txtResultado.setText("");
                    txtResultado.setText(txtResultado.getText().toString() + "7");
                } else {
                    txtResultado.setText(txtResultado.getText().toString() + "7");
                }
            }
        });

        btnNum8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txtResultado.getText().toString().equals("0")) {
                    txtResultado.setText("");
                    txtResultado.setText(txtResultado.getText().toString() + "8");
                } else {
                    txtResultado.setText(txtResultado.getText().toString() + "8");
                }
            }
        });

        btnNum9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txtResultado.getText().toString().equals("0")) {
                    txtResultado.setText("");
                    txtResultado.setText(txtResultado.getText().toString() + "9");
                } else {
                    txtResultado.setText(txtResultado.getText().toString() + "9");
                }
            }
        });

        btnNum0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txtResultado.getText().toString().equals("0")) {

                } else {
                    txtResultado.setText(txtResultado.getText().toString() + "0");
                }
            }
        });


        btnSoma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sinal = "+";

                if(String.valueOf(primeiroNum).equals("0.0")) {
                    primeiroNum = Double.valueOf(txtResultado.getText().toString());
                    txtHistorico.setText(txtResultado.getText().toString() + " + ");
                    txtResultado.setText("");
                }else{
                    segundoNum = Double.valueOf(txtResultado.getText().toString());
                    primeiroNum = Double.valueOf(calculadora());

                    txtHistorico.setText(String.valueOf(primeiroNum) + " + ");
                    txtResultado.setText("");
                }
            }
        });

        btnSubtracao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sinal = "-";

                if(String.valueOf(primeiroNum).equals("0.0")) {
                    primeiroNum = Double.valueOf(txtResultado.getText().toString());
                    txtHistorico.setText(txtResultado.getText().toString() + " - ");
                    txtResultado.setText("");
                }else{
                    segundoNum = Double.valueOf(txtResultado.getText().toString());
                    primeiroNum = Double.valueOf(calculadora());

                    txtHistorico.setText(String.valueOf(primeiroNum) + " - ");
                    txtResultado.setText("");
                }
            }
        });

        btnMultiplicacao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sinal = "*";

                if(String.valueOf(primeiroNum).equals("0.0")) {
                    primeiroNum = Double.valueOf(txtResultado.getText().toString());
                    txtHistorico.setText(txtResultado.getText().toString() + " * ");
                    txtResultado.setText("");
                }else{
                    segundoNum = Double.valueOf(txtResultado.getText().toString());
                    primeiroNum = Double.valueOf(calculadora());

                    txtHistorico.setText(String.valueOf(primeiroNum) + " * ");
                    txtResultado.setText("");
                }
            }
        });

        btnDivisao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sinal = "/";

                if(String.valueOf(primeiroNum).equals("0.0")) {
                    primeiroNum = Double.valueOf(txtResultado.getText().toString());
                    txtHistorico.setText(txtResultado.getText().toString() + " / ");
                    txtResultado.setText("");
                }else{
                    segundoNum = Double.valueOf(txtResultado.getText().toString());
                    primeiroNum = Double.valueOf(calculadora());

                    txtHistorico.setText(String.valueOf(primeiroNum) + " / ");
                    txtResultado.setText("");
                }
            }
        });

        btnCE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(txtResultado.getText().toString().equals("0")){
                    txtResultado.setText("0");
                    txtHistorico.setText(" ");
                    primeiroNum = 0;
                    segundoNum = 0;
                    sinal = " ";
                }else{
                txtResultado.setText("0");}
            }
        });

        btnPonto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtResultado.setText(txtResultado.getText().toString() + ".");
            }
        });


        btnIgual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                segundoNum = Double.valueOf(txtResultado.getText().toString());
                txtHistorico.setText(txtHistorico.getText().toString() + txtResultado.getText().toString());

                txtResultado.setText(calculadora());

            }
        });
    }

    private String calculadora(){
        double resultado = 0;

        switch (sinal){
            case "+":
                resultado = primeiroNum + segundoNum;
                break;

            case "-":
                resultado = primeiroNum - segundoNum;
                break;

            case "*":
                resultado = primeiroNum * segundoNum;
                break;

            case "/":
                resultado = primeiroNum / segundoNum;
                break;
        }
        return String.valueOf(resultado);
    }


}
